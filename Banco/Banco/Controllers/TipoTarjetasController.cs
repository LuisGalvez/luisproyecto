﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Banco.Models;

namespace Banco.Controllers
{
    public class TipoTarjetasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: TipoTarjetas
        [Authorize(Roles = "Admin, User")]
        public ActionResult Index()
        {
            return View(db.TipoTarjetas.ToList());
        }

        // GET: TipoTarjetas/Details/5
        [Authorize(Roles = "Admin, User")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoTarjeta tipoTarjeta = db.TipoTarjetas.Find(id);
            if (tipoTarjeta == null)
            {
                return HttpNotFound();
            }
            return View(tipoTarjeta);
        }

        // GET: TipoTarjetas/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: TipoTarjetas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Tipo")] TipoTarjeta tipoTarjeta)
        {
            if (ModelState.IsValid)
            {
                db.TipoTarjetas.Add(tipoTarjeta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tipoTarjeta);
        }

        // GET: TipoTarjetas/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoTarjeta tipoTarjeta = db.TipoTarjetas.Find(id);
            if (tipoTarjeta == null)
            {
                return HttpNotFound();
            }
            return View(tipoTarjeta);
        }

        // POST: TipoTarjetas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Tipo")] TipoTarjeta tipoTarjeta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipoTarjeta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tipoTarjeta);
        }

        // GET: TipoTarjetas/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoTarjeta tipoTarjeta = db.TipoTarjetas.Find(id);
            if (tipoTarjeta == null)
            {
                return HttpNotFound();
            }
            return View(tipoTarjeta);
        }

        // POST: TipoTarjetas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TipoTarjeta tipoTarjeta = db.TipoTarjetas.Find(id);
            db.TipoTarjetas.Remove(tipoTarjeta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
