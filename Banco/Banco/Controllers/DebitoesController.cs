﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Banco.Models;

namespace Banco.Controllers
{
    public class DebitoesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Debitoes
        [Authorize(Roles = "Admin, User")]
        public ActionResult Index()
        {
            var debitos = db.Debitos.Include(d => d.Cuenta).Include(d => d.TipoTarjeta);
            return View(debitos.ToList());
        }

        // GET: Debitoes/Details/5
        [Authorize(Roles = "Admin, User")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Debito debito = db.Debitos.Find(id);
            if (debito == null)
            {
                return HttpNotFound();
            }
            return View(debito);
        }

        // GET: Debitoes/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            ViewBag.CuentaId = new SelectList(db.Cuentas, "Id", "NumeroCuenta");
            ViewBag.TipoTarjetaId = new SelectList(db.TipoTarjetas, "Id", "Tipo");
            return View();
        }

        // POST: Debitoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,NumeroTarjeta,Pin,Activacioon,Vencimiento,Estado,CuentaId,TipoTarjetaId")] Debito debito)
        {
            if (ModelState.IsValid)
            {
                db.Debitos.Add(debito);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CuentaId = new SelectList(db.Cuentas, "Id", "NumeroCuenta", debito.CuentaId);
            ViewBag.TipoTarjetaId = new SelectList(db.TipoTarjetas, "Id", "Tipo", debito.TipoTarjetaId);
            return View(debito);
        }

        // GET: Debitoes/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Debito debito = db.Debitos.Find(id);
            if (debito == null)
            {
                return HttpNotFound();
            }
            ViewBag.CuentaId = new SelectList(db.Cuentas, "Id", "NumeroCuenta", debito.CuentaId);
            ViewBag.TipoTarjetaId = new SelectList(db.TipoTarjetas, "Id", "Tipo", debito.TipoTarjetaId);
            return View(debito);
        }

        // POST: Debitoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,NumeroTarjeta,Pin,Activacioon,Vencimiento,Estado,CuentaId,TipoTarjetaId")] Debito debito)
        {
            if (ModelState.IsValid)
            {
                db.Entry(debito).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CuentaId = new SelectList(db.Cuentas, "Id", "NumeroCuenta", debito.CuentaId);
            ViewBag.TipoTarjetaId = new SelectList(db.TipoTarjetas, "Id", "Tipo", debito.TipoTarjetaId);
            return View(debito);
        }

        // GET: Debitoes/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Debito debito = db.Debitos.Find(id);
            if (debito == null)
            {
                return HttpNotFound();
            }
            return View(debito);
        }

        // POST: Debitoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Debito debito = db.Debitos.Find(id);
            db.Debitos.Remove(debito);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
