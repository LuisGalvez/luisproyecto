﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Banco.Models;

namespace Banco.Controllers
{
    public class TipoMovimientoesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: TipoMovimientoes
        [Authorize(Roles = "Admin, User")]
        public ActionResult Index()
        {
            return View(db.TipoMovimientos.ToList());
        }

        // GET: TipoMovimientoes/Details/5
        [Authorize(Roles = "Admin, User")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoMovimiento tipoMovimiento = db.TipoMovimientos.Find(id);
            if (tipoMovimiento == null)
            {
                return HttpNotFound();
            }
            return View(tipoMovimiento);
        }

        // GET: TipoMovimientoes/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: TipoMovimientoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Tipo,Descripcion")] TipoMovimiento tipoMovimiento)
        {
            if (ModelState.IsValid)
            {
                db.TipoMovimientos.Add(tipoMovimiento);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tipoMovimiento);
        }

        // GET: TipoMovimientoes/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoMovimiento tipoMovimiento = db.TipoMovimientos.Find(id);
            if (tipoMovimiento == null)
            {
                return HttpNotFound();
            }
            return View(tipoMovimiento);
        }

        // POST: TipoMovimientoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Tipo,Descripcion")] TipoMovimiento tipoMovimiento)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipoMovimiento).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tipoMovimiento);
        }

        // GET: TipoMovimientoes/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoMovimiento tipoMovimiento = db.TipoMovimientos.Find(id);
            if (tipoMovimiento == null)
            {
                return HttpNotFound();
            }
            return View(tipoMovimiento);
        }

        // POST: TipoMovimientoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TipoMovimiento tipoMovimiento = db.TipoMovimientos.Find(id);
            db.TipoMovimientos.Remove(tipoMovimiento);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
