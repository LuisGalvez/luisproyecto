namespace Banco.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Banco.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;

    internal sealed class Configuration : DbMigrationsConfiguration<Banco.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Banco.Models.ApplicationDbContext context)
        {
            if (!context.Users.Any(u => u.UserName == "luisgalvez_8@guatefinanzas.com.gt"))
            {
                var roleStore = new RoleStore<IdentityRole>(context);
                var roleManager = new RoleManager<IdentityRole>(roleStore);

                var store = new UserStore<ApplicationUser>(context);
                var manager = new UserManager<ApplicationUser>(store);
                var user = new ApplicationUser { UserName = "luisgalvez_8@guatefinanzas.com.gt", Email = "luisgalvez_8@guatefinanzas.com.gt" };

                manager.Create(user, "Luis-123");
                var roleAdmin = new IdentityRole { Name = "Admin" };
                roleManager.Create(roleAdmin);
                roleManager.Create(new IdentityRole { Name = "User" });

                manager.AddToRole(user.Id, "Admin");
            }
        }
    }
}
