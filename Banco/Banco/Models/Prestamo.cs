﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Banco.Models
{
    public class Prestamo
    {
        public int Id { get; set; }

        public int Meses { get; set; }
        
        public decimal Monto { get; set; }

        [Required]
        [Display(Name = "FechaInicial")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Fecha { get; set; }

        [Required]
        [Display(Name = "FechaLimite")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime FechaLimite { get; set; }

        public int PersonaId { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }

        public virtual Persona Persona { get; set; }

    }
}