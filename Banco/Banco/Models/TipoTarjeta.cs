﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Banco.Models
{
    public class TipoTarjeta
    {
        public int Id { get; set; }
        public string Tipo { get; set; }
    }
}