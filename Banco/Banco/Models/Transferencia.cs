﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Banco.Models
{
    public class Transferencia
    {
       
       
        public int Id { get; set; }
        public decimal Monto { get; set; }
        public DateTime FechaYHora { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }

        public int CuentaId { get; set; }

        public virtual Cuenta Cuenta { get; set; }

        
    }
}