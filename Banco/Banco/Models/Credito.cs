﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Banco.Models
{
    public class Credito
    {
        public int Id { get; set; }
        public string NumeroTarjeta {get; set;}
        public string Pin { get; set; }
        public decimal Saldo { get; set; }
        public DateTime Activacion { get; set; }
        public DateTime Vencimiento { get; set; }
        public bool Estado { get; set; }

        public int TipoTarjetaId { get; set; }
        public int PersonaId { get; set; }
        public int? CuentaId { get; set; }

        public virtual TipoTarjeta TipoTarjeta { get; set; }
        public virtual Persona Persona { get; set; }
        public virtual Cuenta Cuenta { get; set; }
 
    }
}