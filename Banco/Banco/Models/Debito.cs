﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Banco.Models
{
    public class Debito
    {
        public int Id { get; set; }
        public string NumeroTarjeta { get; set; }
        public string Pin { get; set; }
        public string Activacioon { get; set; }
        public string Vencimiento { get; set; }
        public bool Estado { get; set; }

        public int CuentaId { get; set; }
        public int TipoTarjetaId { get; set; }

        public virtual Cuenta Cuenta { get; set; }
        public virtual TipoTarjeta TipoTarjeta { get; set; }
    }
}